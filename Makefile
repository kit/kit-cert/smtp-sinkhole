.PHONY: install clean

.DEFAULT_GOAL: smtp-sinkhole

smtp-sinkhole: smtp-sinkhole.go
	go build -ldflags "-s -w" -o $@ $<

install: smtp-sinkhole misc/smtp-sinkhole.service
	sudo install smtp-sinkhole /usr/local/sbin/
	sudo setcap cap_net_admin,cap_net_bind_service+ep /usr/local/sbin/smtp-sinkhole
	sudo install -m 644 misc/smtp-sinkhole.service /etc/systemd/system
	sudo systemctl daemon-reload

clean:
	rm -f smtp-sinkhole
