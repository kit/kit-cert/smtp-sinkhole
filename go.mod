module gitlab.kit.edu/kit/kit-cert/smtp-sinkhole

go 1.19

require (
	github.com/LiamHaworth/go-tproxy v0.0.0-20190726054950-ef7efd7f24ed
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/satori/go.uuid v1.2.0
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
