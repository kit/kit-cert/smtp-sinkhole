# smtp-sinkhole [![pipeline status](https://gitlab.kit.edu/kit/kit-cert/smtp-sinkhole/badges/master/pipeline.svg)](https://gitlab.kit.edu/kit/kit-cert/smtp-sinkhole/commits/master)

smtp-sinkhole accepts SMTP connections, prints a specific statement (which is
usually shown to mail user agents), signals an error and terminates the
connection after a short time.

## Prerequisite

Redirect all prohibited smtp traffic to a machine running smtp-sinkhole.

## Installation

This part is Debian-specific.

* Install [Google Go](https://golang.org)
* Install libcap-tools: `apt install -y libcap2-bin`
* Install sinkhole:
```
git clone https://gitlab.kit.edu/kit/kit-cert/smtp-sinkhole.git
cd smtp-sinkhole
make install
systemctl enable --now smtp-sinkhole.service
```

* Configure Netfilter:
```
apt install -y iptables-persistent
```
```
ip6tables -t mangle -N DIVERT
ip6tables -t mangle -A PREROUTING -p tcp -m tcp --dport 25 -j DIVERT
ip6tables -t mangle -A DIVERT -j MARK --set-xmark 0x1/0xffffffff # set mark to enable custom routing table
ip6tables -t mangle -A DIVERT -j ACCEPT
ip -6 route add local ::/0 dev lo table 100
ip -6 rule add fwmark 1 lookup 100
```
```
iptables -t mangle -N DIVERT
iptables -t mangle -A PREROUTING -p tcp -m tcp --dport 25 -j DIVERT
iptables -t mangle -A DIVERT -j MARK --set-xmark 0x1/0xffffffff # set mark to enable custom routing table
iptables -t mangle -A DIVERT -j ACCEPT
ip route add local 0.0.0.0/0 dev lo table 100
ip rule add fwmark 1 lookup 100
```

```
ip6tables-save > /etc/iptables/rules.v6
iptables-save > /etc/iptables/rules.v4
```

Make sure the rules and routes are persistent. When using the
traditional Debian network configuration, prepend all lines
starting with `ip` with `post-up ` and add them to the
appropriate interface definitions in `/etc/network/interfaces`.

## Links

See the [Kernel's TPROXY documentation](https://docs.kernel.org/networking/tproxy.html) for details.
