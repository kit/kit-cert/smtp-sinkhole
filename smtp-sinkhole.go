package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/LiamHaworth/go-tproxy"
	"github.com/coreos/go-systemd/daemon"
	"github.com/coreos/go-systemd/journal"
	"github.com/satori/go.uuid"
)

// Main SMTP Message Template. Don't change the ???. at the beginning of every line.
const (
	smtpNoMailTemplate = `???-Externer Mailversand per SMTP ist im Netz des KIT geblockt!
???-Bitte verwenden Sie die Mailserver des SCC; Hilfe und Informationen bekommen Sie beim
??? ServiceDesk unter +49 (721) 608-8000 und auf https://www.scc.kit.edu/sl/sendmail
`
	DefaultSMTPTimeout = 600
)

var (
	// precomputing some strings
	smtpNoMail554   = []byte(strings.Replace(smtpNoMailTemplate, "???", "554", -1))
	smtpNoMail504   = []byte(strings.Replace(smtpNoMailTemplate, "???", "504", -1))
	smtpHelpMessage = []byte(strings.Replace(smtpNoMailTemplate, "???", "214", -1))
	// global connection map
	IPConnCounter     = make(map[uint64]int)
	IPConnCounterLock sync.RWMutex
	// channel used for reception of OS signals
	signalChan    = make(chan os.Signal)
	listenAddress *net.TCPAddr
	// configuration
	confLocalPort string
)

func parseCommandline() {
	const (
		bindHelp = "Address to bind to; using format HOST:PORT."
	)
	flag.StringVar(&confLocalPort, "port", "25", bindHelp)
	flag.StringVar(&confLocalPort, "p", "25", bindHelp+"(shorthand)")
	needHelp := flag.Bool("help", false, "Need help?")
	flag.Parse()
	// print help
	if *needHelp == true {
		flag.PrintDefaults()
		os.Exit(127)
	}
	// parse port
	port, err := strconv.Atoi(confLocalPort)
	if err != nil {
		log.Fatalf("Unable to parse port: %s", err)
	}
	listenAddress = &net.TCPAddr{
		IP:   net.IPv6zero,
		Port: port,
	}
}

// early initialisation
func init() {
	// test journald support
	if journal.Enabled() == false {
		log.Println("Unable to connect to journald")
	}
	// parse commandline flags
	parseCommandline()
	// start periodic cleanup of connection hash
	go func() {
		for {
			time.Sleep(300 * time.Second)
			expireConnectionTimeouts()
		}
	}()
	// react on signals
	signal.Notify(signalChan, syscall.SIGUSR1)
	go func() {
		for s := range signalChan {
			fmt.Printf("%T\t%#v\n", s, s)
			IPConnCounterLock.Lock()
			fmt.Printf("Connectiontable: %#v\n", IPConnCounter)
			IPConnCounterLock.Unlock()
		}
	}()
}

// decrease connection counters
func expireConnectionTimeouts() {
	IPConnCounterLock.Lock()
	defer IPConnCounterLock.Unlock()
	for k, v := range IPConnCounter {
		IPConnCounter[k] = v / 2
	}
}

// add new connection endpoint to global connection map
func newConnectionGetTimeout(host net.Addr) int {
	IPString, _, err := net.SplitHostPort(host.String())
	if err == nil {
		// if host is not parsable, use the multicast reserved base address as
		// default placeholder
		IPString = "224.0.0.0"
	}
	// this is a dirty hack and a sh**ty hash function
	ip := net.ParseIP(IPString).To16()
	var sum, exp uint64 = 0, 1
	for idx := 0; idx < len(ip); idx++ {
		sum += exp * uint64(ip[idx])
		exp *= 255
	}

	// Lock map
	IPConnCounterLock.Lock()
	defer IPConnCounterLock.Unlock()
	IPConnCounter[sum]++
	//fmt.Printf("%#v\n", IPConnCounter)
	return DefaultSMTPTimeout / IPConnCounter[sum]
}

func doSMTP(conn net.Conn) {
	var (
		sessionID string
	)
	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	remoteAddr := conn.RemoteAddr().String()
	sessionID = uuid.NewV4().String()

	_ = journal.Send(
		fmt.Sprintf("[%s] CONN %s => %s", sessionID, remoteAddr, localAddr),
		journal.PriInfo,
		map[string]string{
			"SESSIONID":  sessionID,
			"LOCALADDR":  localAddr,
			"REMOTEADDR": remoteAddr,
		})

	/*
	   This is compatible with RFC 2821, especially paragraph 4.3.2
	*/
	_, _ = conn.Write(smtpNoMail554)
	var readBuf = make([]byte, 9000)
	for {
		// reset timeout
		timeout := newConnectionGetTimeout(conn.LocalAddr()) // move this out of the loop to increase response time
		_ = conn.SetDeadline(time.Now().Add(time.Duration(timeout) * time.Second))
		// read next line
		_, err := conn.Read(readBuf)
		answer := string(readBuf)
		if err == nil {
			answerFields := strings.Fields(answer)
			switch strings.ToLower(answerFields[0]) {
			case "quit":
				_, _ = conn.Write([]byte("221 OK sinkhole closing connection\n"))
				return
			case "ehlo", "helo":
				_, _ = conn.Write(smtpNoMail504)
			case "noop", "rset":
				_, _ = conn.Write([]byte("250 Requested mail action okay, completed\n"))
			case "expn", "vrfy":
				_, _ = conn.Write([]byte("504 Command parameter not implemented\n"))
			case "help":
				_, _ = conn.Write(smtpHelpMessage)
			default: // MAIL, DATA,
				_, _ = conn.Write([]byte("503 bad sequence of commands\n"))
			}
		} else {
			// TODO better handling of timeouts and other errors
			errorMessage := err.Error()
			_ = journal.Send(
				fmt.Sprintf("[%s] ERR %s", sessionID, errorMessage),
				journal.PriInfo,
				map[string]string{
					"SESSIONID":  sessionID,
					"LOCALADDR":  localAddr,
					"REMOTEADDR": remoteAddr,
				})
			return
		}
	}
}

func main() {
	_ = journal.Print(journal.PriInfo, "Starting sinkhole smtp server at %s", confLocalPort)

	// accept new connections
	// TODO https://bravenewmethod.wordpress.com/2011/03/17/interpreting-go-socket-errors/
	listener, err := tproxy.ListenTCP("tcp", listenAddress)
	if err != nil {
		_ = journal.Print(journal.PriErr, "Error listening: %s", err.Error())
		os.Exit(127)
	}
	// signal readiness to systemd
	_, _ = daemon.SdNotify(false, "READY=1")

	for {
		conn, err := listener.Accept()
		if err != nil {
			_ = journal.Print(journal.PriErr, "Error accepting connection: %s", err.Error())
			return
		}
		// handle smtp protocol for new client
		go doSMTP(conn)
	}
}
